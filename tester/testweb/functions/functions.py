# -*- coding: utf-8 -*-

import sys
import os
from subprocess import call
from os import getcwd , remove
from time import sleep as t
from random import choice, randint

from terminaltables import AsciiTable as Table
from termcolor import colored, cprint
from selenium import webdriver as wd
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import WebDriverException
from selenium.webdriver.chrome.options import Options

import constants.constants as c

def errorMsg(var):
    print(red("**ERROR: Not found:\n %s\n" % var), file=sys.stderr)

def findBySelector(driver, key):
    counter = 0
    while True:
        try:
            element = driver.find_element_by_css_selector(key)
            return element
            break
        except Exception as e:
            t(2)
            counter += 1
            if counter >= 5:
                errorMsg(key)
                quitExecution(driver)

def writeById(driver, key, text):
    count = 0
    while True:
        try:
            element = driver.find_element_by_id(key)
            element.clear()
            element.send_keys(text)
            if isSet(element):
                break
            else:
                t(2)
        except Exception as e:
            print(e)
            t(2)
            count+=1
            if count >= 5:
                errorMsg(key)
                quitExecution(driver)

def writeBySelector(driver, key, text):
    count = 0
    while True:
        try:
            element = driver.find_element_by_css_selector(key)
            element.clear()
            element.send_keys(text)
            if isSet(element):
                break
        except:
            t(2)
            count+=1
            if count >= 5:
                errorMsg(key)
                quitExecution(driver)

def isSet(element):
    if len(element.get_attribute('value')) > 0:
        return True
    else:
        return False

def clickBySelector(driver, key):
    counter = 0
    while True:
        try:
            element = driver.find_element_by_css_selector(key)
            element.click()
            break
        except Exception as e:
            t(2)
            counter += 1
            if counter == 5:
               errorMsg(key)
               quitExecution(driver)

def clickByLink(driver, link):
    counter = 0
    while True:
        try:
            element = driver.find_element_by_link_text(link)
            element.click()
            break
        except Exception as e:
            t(2)
            counter += 1
            if counter == 5:
                errorMsg(link)
                quitExecution(driver)

def clickById(driver, key):
    counter = 0
    while True:
        try:
            element = driver.find_element_by_id(key)
            element.click()
            break
        except Exception as e:
            t(2)
            counter += 1
            if counter == 5:
               errorMsg(key)
               quitExecution(driver)

def clickByTag(driver, tag, buttonName):
    count = 0
    flag = False
    while not flag:
        try:
            elements = driver.find_elements_by_tag_name(tag)
            for i in range(0, len(elements)):
                #print "%s -> %s" % (elements[i].text.encode('utf-8'), i)
                if elements[i].text.encode('utf-8') == buttonName.encode('utf-8'):
                    elements[i].click()
                    flag = True
                    break
        except:
            t(2)
            count +=1
            if count == 5:
                errorMsg(buttonName)
                quitExecution(driver)

def selectByText(driver, key, text):
    count = 0
    while True:
        try:
            select = Select(driver.find_element_by_css_selector(key))
            #print([options.text.encode('utf-8') for options in select.options])
            select.select_by_visible_text(text)
            if select.first_selected_option.text == text:
                break
            else:
                print ("Inyectado texto en: %s - %s:%i" %
                       (key, select.first_selected_option.text, count))
        except:
            t(2)
            count +=1
            if count == 10:
                errorMsg(key)
                quitExecution(driver)

def selectByIndex(driver, key, index):
    count = 0
    while True:
        try:
            select = Select(driver.find_element_by_css_selector(key))
            select.options[index].click()
            if int(select.options[index].get_attribute('index')) == int(index):
                break
        except Exception as e:
            t(2)
            count +=1
            if count == 5:
                errorMsg(key)
                quitExecution(driver)

def wait(wd, selector,timeout=40,condition=EC.presence_of_element_located,by=By.CSS_SELECTOR):
    element = WebDriverWait(wd, timeout).until(condition((by,selector)))

def getScreenshot(browser, name):
    t(1.5)
    image = "%s/%s_%s" % (c.screenshotPathToSave, c.build_number, name)
    browser.get_screenshot_as_file(image)
    return image

def quitExecution(driver):
    #image = getScreenshot(driver, 'LastImage.png')
    #print(driver.current_url)
    #print ("%s\n%s\n" % (red('**ERROR:'), red(image)), file=sys.stderr)
    assert False

def setWebDriver(browser):
    driver = wd.Chrome(c.chromeDriverPath)
    driver.implicitly_wait(5)
    driver.set_window_position(0, 0)
    driver.set_window_size(1024, 768)
    return driver

def resultTable(outputList):
    if len(outputList) > 1:
        out = Table(outputList)
        out.inner_row_border = True
        for i in range(len(outputList[0])): out.justify_columns[i] = 'center'
        print("\n%s\n" % out.table)

def setOutputTable(*headers):
    output = []
    output.append(headers[0])
    return output

def blue(var):
    return colored(var, 'blue', attrs=['bold'])

def green(var):
    return colored(var, 'green', attrs=['bold'])

def grey(var):
    return colored(var, 'grey', attrs=['bold'])

def red(var):
    return colored(var, 'red', attrs=['bold'])

def white(var):
    return colored(var, 'white', attrs=['bold'])

def createScreenDirs():
	if not os.path.exists(c.screenshotPathToSave):
		os.makedirs(c.screenshotPathToSave)
		call(['chmod', '0777', c.screenshotPathToSave])
