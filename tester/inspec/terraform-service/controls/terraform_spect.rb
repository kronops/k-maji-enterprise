# encoding: utf-8

describe file('/bin/aws') do
  it { should exist }
end

describe file('/usr/local/bin/terraform') do
  it { should exist }
end
