nginx-service
=============

Playbook to deploy nginx http server.

Requirements
------------

EL based system.

Role Variables
--------------

Dependencies
------------

Example Playbook
----------------

To use this role, just include it in your playbook, for example:

    - hosts: servers
      roles:
         - nginx-service

License
-------

MIT

Author Information
------------------

Please any question, please contact the author at: jorge.medina@kronops.com.mx.
