# Building QEMU-KVM Linux Server Images with Packer

## Introduction

This project helps you build server images with the server templating tool Packer from Hashicorp,
Packer is a tool for creating machine and container images for multiple platforms from a single
source configuration.

The follow is a list of main features:

 * It is easy to use and automates the creation of any type of machine image.
 * Embraces modern configuration management to install and configure software within images.
 * Multiple virtualization providers: Virtualbox, QEMU, Amazon EC2, Docker and more.

We are going to use packer to build machine images for K-Maji Orquesta services, we will use
the QEMU provider to create kvm-qemu machines for local and cloud environments. These machine
images will be based on Ubuntu 16.04 on 64-bit (Xenial) and RHEL7 based like CentOS7 and
OracleLinux7.

## Objetives

We are going to build the images manually on a linux host server, normally this is run
from jenkins. The follow is a list of objetives:

 * Build server images for Ubuntu 16.04 and CentOS7 on 64-bit for QEMU.
 * Provision server images with shell scripts and configuration management tools.
 * Publish the server image template artifact on shared repository.

## Requirements

In order to programatically build a ubuntu-16.04 64-bit server image from the ground, you will
need a physical machine with virtualization support, for example Intel VT or AMD-V with the follow:

* Linux (ubunut-16.04 based)
* Packer 1.2.x
* QEMU 2.5
* ISO media
* Preseed file
* Packer jason file
* Provision scripts

We don't cover the setup instructions for this components. It is recommended that you deploy
kvm-service and kvm-templates roles from Ansible using the playbooks/deploy-virthost.yml.

## Structure

We need to be sure we have a directory structure like this inside the server-images directory:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$tree -f
.
├── ./bin
│   ├── ./bin/rhel-cleanup.sh
│   ├── ./bin/rhel-kernel.sh
│   ├── ./bin/rhel-sshd.sh
│   ├── ./bin/rhel-update.sh
│   ├── ./bin/ubuntu-bootstrap.sh
│   └── ./bin/ubuntu-disable-apt-daily.sh
├── ./centos7.json
├── ./http
│   ├── ./http/centos7.cfg
│   ├── ./http/oraclelinux6.cfg
│   ├── ./http/oraclelinux7.cfg
│   ├── ./http/preseed.cfg
│   └── ./http/rhel7.cfg
├── ./iso
│   └── ./iso/empty
├── ./Makefile
├── ./oraclelinux6.json
├── ./oraclelinux7.json
├── ./README.md
├── ./rhel7.json
└── ./ubuntu1604.json

3 directories, 19 files
```

### JSON files

We can see that at the root of **server-images** there are a few **.json** files. These files
are part of the input packer uses, this is where packer read build and provision
instructions.

Every json files is composed of blocks, this are:

* builders
* provisions
* variables

In **builders** you define the options to build the virtual image for a specific hypervisor
provider, we use qemu, in this block we define options like:

* type: hypervisor type
* accelerator: accelerate with kvm.
* qemu_binary: path to qemu-kvm binary.
* vm_name: custom name.
* disk_interface: diks driver for accelerated.
* disk_size: file image size.
* format: file image format.
* headless: don't need monitor output, it is only console.
* net_device: virtio-net for accelerated image.
* iso_urls: web url to iso.
* iso_checksum: checksum string for verification.
* iso_checksum_type: alghoritm to checksum.
* http_directory: directory to find preseed and kickstart files.
* boot_command: commands to run the os installer.
* boot_wait: time to wait for boot installer.
* shutdown_command: command to shutdown after finish build and provision process.
* ssh_password: password to login inside machine.
* ssh_username: username to login inside machine.
* ssh_port: port listening inside machine.
* ssh_wait_timeout: time to wait after install and restart.
* output_directory: path to image file.
* qemuargs: Custom parameters, like cpus and memory.

The **provisioners** block mostly shell scripts to run inside the image operating system.

The **variables** block defines common variables and their defaults, use this at your own risk :).

### HTTP directory

Now, let's check inside the http directory, this directory holds automatic operating system
installation scripts, for example, preseeds for Debian/Ubuntu and kickstarts for RHEL7 or
CentOS7.

This project support following:

```shell
ls -l http
total 24
-rw-rw-r-- 1 jmedina jmedina 2568 jun  1 22:11 centos7.cfg
-rw-rw-r-- 1 jmedina jmedina 2383 feb 23 17:18 oraclelinux6.cfg
-rw-rw-r-- 1 jmedina jmedina 2573 jun  1 22:11 oraclelinux7.cfg
-rw-rw-r-- 1 jmedina jmedina 1822 feb 23 17:18 preseed.cfg
-rw-rw-r-- 1 jmedina jmedina 2566 jun  1 22:09 rhel7.cfg
```

In json file, inside **buiders** block, we should define **http_directory** and **boot_command**,
we use this for ubuntu1604:

```
"http_directory": "http",
"boot_command": [
  "<enter><wait><f6><esc><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
  "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
  "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
  "<bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs><bs>",
  "/install/vmlinuz<wait>",
  " auto=true<wait>",
  " priority=critical<wait>",
  " initrd=/install/initrd.gz<wait>",
  " preseed/url=http://{{ .HTTPIP }}:{{ .HTTPPort }}/{{ user `preseed` }}<wait>",
  " -- <wait>",
  "<enter><wait>"
],
```

As you can see in the **preseed/url** we use **{{ user `preseed` }}** to substite the path
to the preseed file name, in **variables** block we predefine **preseed**:

```
    "preseed":           "preseed.cfg",
```

### ISO directory

We store the installation media in the **iso** directory, if you store your iso files on another
location, you can symlink, for example:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ ln -s /kvm/isos/ubuntu-16.04.5-server-amd64.iso iso
$ ls -l iso/
total 0
-rw-rw-r-- 1 jmedina jmedina  0 feb 23 17:18 empty
lrwxrwxrwx 1 jmedina jmedina 869269504 jun  2 09:57 ubuntu-16.04.2-server-amd64.iso -> /kvm/isos/ubuntu-16.04.2-server-amd64.iso
```

!!! important "If you haven't had downloaded the iso, don't worry, packer will download it for you."

!!! important "Be sure the ISO's sha256 checksum matches the one on the .json file."

## Usage

First, we need to be sure we are located at the **server-images** directory:

```shell
[sysadmin@linux-kvm-build][~]
$ cd ~/vcs/kronops/k-maji-orquesta/server-images
```

We validate a individual template:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ packer validate ubuntu-16.04.json
Template validated successfully.
```

To validate all templates you can use **make validate**:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ make validate
packer validate ubuntu1604.json
Template validated successfully.
packer validate centos7.json
Template validated successfully.
packer validate rhel7.json
Template validated successfully.
packer validate oraclelinux7.json
Template validated successfully.
packer validate oraclelinux6.json
Template validated successfully.
```

And now build the ubuntu1604 image manually:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ packer build ubuntu1604.json
```

You can also use **make build-ubuntu1604**:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ make build-ubuntu1604
```

The output will be something like the follow:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ make build-ubuntu1604
packer build ubuntu1604.json
qemu output will be in this color.

==> qemu: Downloading or copying ISO
    qemu: Downloading or copying: file:///kvm/isos/ubuntu-16.04.5-server-amd64.iso
==> qemu: Creating hard drive...
==> qemu: Starting HTTP server on port 8680
==> qemu: Found port for communicator (SSH, WinRM, etc): 3247.
==> qemu: Looking for available port between 5900 and 6000 on 127.0.0.1
==> qemu: Starting VM, booting from CD-ROM
    qemu: The VM will be run headless, without a GUI. If you want to
    qemu: view the screen of the VM, connect via VNC without a password to
    qemu: vnc://127.0.0.1:5937[0m
==> qemu: Overriding defaults Qemu arguments with QemuArgs...
==> qemu: Waiting 40s for boot...
==> qemu: Connecting to VM via VNC
==> qemu: Typing the boot command over VNC...
==> qemu: Waiting for SSH to become available...
==> qemu: Connected to SSH!
==> qemu: Provisioning with shell script: bin/ubuntu-disable-apt-daily.sh
    qemu: [sudo] password for sysadmin:
==> qemu: Gracefully halting virtual machine...
    qemu: [sudo] password for sysadmin:
==> qemu: Converting hard drive...
Build 'qemu' finished.

==> Builds finished. The artifacts of successful builds are:
--> qemu: VM files in directory: builds/libvirt/template-ubuntu1604-x64-server.qcow2
Finished: SUCCESS
```

The output of packer building centos7 image is like the following:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ make build-centos7
packer build centos7.json
qemu output will be in this color.

==> qemu: Downloading or copying ISO
    qemu: Downloading or copying: file:///kvm/isos/CentOS-7-x86_64-DVD-1810.iso
==> qemu: Creating hard drive...
==> qemu: Starting HTTP server on port 8837
==> qemu: Found port for communicator (SSH, WinRM, etc): 4152.
==> qemu: Looking for available port between 5900 and 6000 on 127.0.0.1
==> qemu: Starting VM, booting from CD-ROM
    qemu: The VM will be run headless, without a GUI. If you want to
    qemu: view the screen of the VM, connect via VNC without a password to
    qemu: vnc://127.0.0.1:5934[0m
==> qemu: Overriding defaults Qemu arguments with QemuArgs...
==> qemu: Waiting 40s for boot...
==> qemu: Connecting to VM via VNC
==> qemu: Typing the boot command over VNC...
==> qemu: Waiting for SSH to become available...
==> qemu: Connected to SSH!
==> qemu: Provisioning with shell script: bin/rhel-cleanup.sh
==> qemu: Gracefully halting virtual machine...
==> qemu: Converting hard drive...
Build 'qemu' finished.

==> Builds finished. The artifacts of successful builds are:
--> qemu: VM files in directory: builds/libvirt/template-centos7-x64-server.qcow2
Finished: SUCCESS
```

IMPORTANT: In case you get errors, **export PACKER_LOG=1** for more info.

The result is the following qcow2 image for libvirt in the builds directory:

[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ ls -lh server-images/builds/libvirt/template-ubuntu1604-x64-server.qcow2/
total 1.8G
-rw-r--r-- 1 jenkins jenkins 1.8G jun  2 10:53 template-ubuntu1604-x64-server.qcow2
```

For centos7, this is:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
 ls -lh server-images/builds/libvirt/template-centos7-x64-server.qcow2/
total 1.5G
-rw-r--r-- 1 jenkins jenkins 1.5G jun  2 15:04 template-centos7-x64-server.qcow2
```

Now, we need to publish this new qcow2 image to a central repository, in this case
**/kvm/staging/server-images**.

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ make publish-ubuntu1604
cp builds/libvirt/template-ubuntu1604-x64-server.qcow2/template-ubuntu1604-x64-server.qcow2 /kvm/staging/server-images
```

This command makes a copy of new image to **/kvm/staging/server-images** so kvm can use the disks.

If you want to publish centos7 image to staging área use this:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ make publish-centos7
$ cp builds/libvirt/template-centos7-x64-server.qcow2/template-centos7-x64-server.qcow2 /kvm/staging/server-images
```

That is, now we have 2 templates:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ ls -lh /kvm/staging/server-images/
-rw-r--r-- 1 jenkins jenkins 1.5G jun  2 15:04 template-centos7-x64-server.qcow2
-rw-r--r-- 1 jenkins jenkins 1.8G jun  2 10:53 template-ubuntu1604-x64-server.qcow2
```

## Convert Images

If you want to convert this qcow2 image to another format, for example for compatibility or migration.

We use **qemu-img command** with convert flag to convert this qcow2 image version2 to qcow2 version1, This
helps to run this images on rhel6 ubuntu1404 hypervisors.

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ qemu-img convert -f qcow2 -O qcow2 -o compat=0.10 \
/kvm/staging/server-images/template-ubuntu1604-x64-server.qcow2 \
/kvm/staging/server-images/template-ubuntu1604-x64-server.qcow
```

To convert centos7 image:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ qemu-img convert -f qcow2 -O qcow2 -o compat=0.10 \
/kvm/staging/server-images/template-centos7-x64-server.qcow2 \
/kvm/staging/server-images/template-centos7-x64-server.qcow
```

Now, let's use **qemu-img info** to see metadata from image file and see the versions:

```shell
[sysadmin@linux-kvm-build][~/vcs/kronops/k-maji-orquesta/server-images]
$ cd /kvm/staging/server-images

$ ls -l
total 6728480
-rw-r--r-- 1 jenkins jenkins 1542979584 jun  2 15:05 template-centos7-x64-server.qcow
-rw-r--r-- 1 jenkins jenkins 1542979584 jun  2 15:04 template-centos7-x64-server.qcow2
-rw-r--r-- 1 jenkins jenkins 1902116864 jun  2 10:54 template-ubuntu1604-x64-server.qcow
-rw-r--r-- 1 jenkins jenkins 1902116864 jun  2 10:53 template-ubuntu1604-x64-server.qcow2

$ qemu-img info template-ubuntu1604-x64-server.qcow2
image: template-ubuntu1604-x64-server.qcow2
file format: qcow2
virtual size: 10G (10737418240 bytes)
disk size: 1.8G
cluster_size: 65536
Format specific information:
    compat: 1.1
    lazy refcounts: false
    refcount bits: 16
    corrupt: false
$ qemu-img info template-ubuntu1604-x64-server.qcow
image: template-ubuntu1604-x64-server.qcow
file format: qcow2
virtual size: 10G (10737418240 bytes)
disk size: 1.8G
cluster_size: 65536
Format specific information:
    compat: 0.10
    refcount bits: 16
```

## Packer and jenkins

If you are going to automate the build and publish image process using jenkins,
you should remember to add user **jenkins** to **kvm** and **libvirtd** system groups, like
this:

```shell
$ id jenkins
uid=1005(jenkins) gid=1005(jenkins) groups=1005(jenkins),117(kvm),118(libvirtd)
```

Create a new job that runs on the linux kvm slave, and then add a shell step
with the following:

```shell
#export PACKER_LOG=1
cd server-images
ln -s /kvm/isos/ubuntu-16.04.5-server-amd64.iso iso
make build-ubuntu1604
make publish-ubuntu1604
```

## References

 * Packer: https://www.packer.io
 * Vagrant: https://www.vagrantup.com
 * Ansible: https://www.ansible.com
 * Build and Image: https://www.packer.io/intro/getting-started/build-image.html
 * https://github.com/sotayamashita/packer-example/blob/master/packer/vagrant/base.json
 * https://github.com/p0bailey/packer-templates
 * https://github.com/p0bailey/ansible-packer/blob/master/tasks/main.yml
