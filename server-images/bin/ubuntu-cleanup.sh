#!/bin/bash

#
# script: ubuntu-cleanup.sh
# author: jorge.medina@kronops.com.mx
# desc: Clean temporary packages, settings and files

# Enable debug mode and log to file
export DEBUG=1
LOG=/var/log/ubuntu-cleanup.log
[ -n "$DEBUG" ] && exec < /dev/stdin > $LOG 2>&1

# Bash debug mode
[ -n "$DEBUG" ] && set -x

# Stop on errors
#set -e

# main

echo "==> Cleaning up udev network interfaces rules"
rm -rf /dev/.udev/
rm /lib/udev/rules.d/75-persistent-net-generator.rules

echo "==> Clean up dhcp leases"
if [ -d "/var/lib/dhcp" ]; then
    rm /var/lib/dhcp/*
fi

echo "==> Cleanup apt cache"
apt-get -y autoremove --purge
apt-get -y clean
apt-get -y autoclean

echo "==> Remove Bash history"
unset HISTFILE
rm -f /root/.bash_history
rm -f /home/sysadmin/.bash_history

echo "==> Removing /tmp/* files"
rm -rf /tmp/*

echo "==> Removing /var/log/*.log files"
find /var/log/ -name *.log -exec rm -f {} \;

echo "==> Clean last login events"
>/var/log/lastlog
>/var/log/wtmp
>/var/log/btmp
